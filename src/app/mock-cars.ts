import { Car } from './car';

export const CARS: Car[] = [
    { vin: 'AABA', year: 2010,	brand: 'BMW', color: 'Red', prices: 390000, type: 'Convert'},
    { vin: 'AABB', year: 2011,	brand: 'BMW', color: 'Black', prices: 102000, type: 'Sport'},
    { vin: 'AABC', year: 2012,	brand: 'BMW', color: 'Gray', prices: 320000, type: 'MPV'},
    { vin: 'AABD', year: 2033,	brand: 'BMW', color: 'Yellow', prices: 270000, type: 'Sedan'},
    { vin: 'AABE', year: 2014,	brand: 'FIAT', color: 'White', prices: 230000, type: 'JEEP'},
    { vin: 'AABF', year: 2015,	brand: 'FIAT', color: 'Silver', prices: 290000, type: 'Sedan'},
    { vin: 'AABG', year: 2016,	brand: 'FIAT', color: 'Blue', prices: 190000, type: 'Sport'},
    { vin: 'AABH', year: 2013,	brand: 'SUZUKI', color: 'Red', prices: 421000, type: 'Convert'},
    { vin: 'AABI', year: 2000,	brand: 'HONDA', color: 'Black', prices: 359000, type: 'JEEP'},
    { vin: 'AABJ', year: 2013,	brand: 'HONDA', color: 'Red', prices: 102500, type: 'MPV'}
    ];
