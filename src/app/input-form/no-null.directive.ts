import { Directive } from '@angular/core';
import { NoNullValidator } from './input-form.component';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appNoNull]',
  providers: [{ provide: NG_VALIDATORS, useExisting: NoNullDirective, multi: true }]
})
export class NoNullDirective implements Validator {
  constructor() { }
  private valFn = NoNullValidator();
    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }

}
